<?php
/**
 * Copyright (c) 2011 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="midcolumn">
	<h2 id="staff-members">
		<a class="anchor" href="#staff"></a><a class="link" href="#staff">Staff Members</a>
	</h2>
	<div class="media clearfix">
		<div class="col-xs-5 col-sm-4">
			<a name="" href="https://www.eclipse.org/user/mbarbero">
				<img class="img-thumbnail img-responsive margin-top-20" src="https://www.eclipse.org/org/foundation/images_staff/mikael.jpg" alt="Mikaël Barbero">
			</a>
		</div>
		<div class="col-xs-19 col-sm-20">
			<div class="media-body">
				<h3 class="margin-bottom-0">
					<a href="https://www.eclipse.org/user/mbarbero">
						Mikaël Barbero 
						<i class="fa fa-link" aria-hidden="true"></i>
					</a>
				</h3>
				<p></p>
				<dl>
					<dt><a href="https://accounts.eclipse.org/users/mbarbero">
						<i class="fa fa-user"></i> mbarbero (eclipse.org)
					</a></dt>
					<dt><a href="https://gitlab.eclipse.org/mbarbero">
						<i class="fa fa-gitlab"></i> mbarbero (gitlab.eclipse.org)
					</a></dt>
					<dt><a href="https://github.com/mbarbero">
						<i class="fa fa-github"></i> mbarbero (github.com)
					</a></dt>
					<dt><a href="https://twitter.com/mikbarbero">
						<i class="fa fa-twitter"></i> @mikbarbero
                	</a></dt>
					<dt><a href="https://hachyderm.io/@mbarbero">
						<i class="fa fa-share-square-o"></i> @mbarbero@hachyderm.io
					</a></dt>
					<dt><a href="https://mikael.barbero.tech">
						<i class="fa fa-safari"></i> mikael.barbero.tech
					</a></dt>
					<dt><a href="mailto:mikael.barbero@eclipse-foundation.org">
						<i class="fa fa-envelope-o"></i> mikael.barbero@eclipse-foundation.org
					</a></dt>
					<dt><a href="https://keys.openpgp.org/search?q=3FAFB6A746AB3E21FDE0E9E8E1E65F0B59A4C4FB">
						<i class="fa fa-id-card-o"></i> 3FAF B6A7 46AB 3E21 FDE0 E9E8 E1E6 5F0B 59A4 C4FB
					</a></dt>
				</dl>
			</div>
		</div>
	</div>
	<hr/>

	<div class="media clearfix">
		<div class="col-xs-5 col-sm-4">
			<a name="" href="https://www.eclipse.org/user/tiagolucas">
				<img class="img-thumbnail img-responsive margin-top-20" src="https://www.eclipse.org/org/foundation/images_staff/tiago-lucas.jpg" alt="Tiago Lucas">
			</a>
		</div>
		<div class="col-xs-19 col-sm-20">
			<div class="media-body">
				<h3 class="margin-bottom-0">
					<a href="https://www.eclipse.org/user/tiagolucas">
						Tiago Lucas 
						<i class="fa fa-link" aria-hidden="true"></i>
					</a>
				</h3>
				<p></p>
				<dl>
					<dt><a href="https://accounts.eclipse.org/users/tiagolucas">
						<i class="fa fa-user"></i> tiagolucas (eclipse.org)
					</a></dt>
					<dt><a href="https://gitlab.eclipse.org/tiagolucas">
						<i class="fa fa-gitlab"></i> tiagolucas (gitlab.eclipse.org)
					</a></dt>
					<dt><a href="https://github.com/TiagoLucas22478">
						<i class="fa fa-github"></i> TiagoLucas22478 (github.com)
					</a></dt>
					<dt><a href="https://tiagoslucas.medium.com">
						<i class="fa fa-safari"></i> tiagoslucas.medium.com
					</a></dt>
					<dt><a href="mailto:tiago.lucas@eclipse-foundation.org">
						<i class="fa fa-envelope-o"></i> tiago.lucas@eclipse-foundation.org
					</a></dt>
				</dl>
			</div>
		</div>
	</div>
	<hr/>

	<div class="media clearfix">
		<div class="col-xs-5 col-sm-4">
			<a name="" href="https://www.eclipse.org/user/netomi">
				<img class="img-thumbnail img-responsive margin-top-20" src="https://www.eclipse.org/org/foundation/images_staff/thomas-neidhart.png" alt="Thomas Neidhart">
			</a>
		</div>
		<div class="col-xs-19 col-sm-20">
			<div class="media-body">
				<h3 class="margin-bottom-0">
					<a href="https://www.eclipse.org/user/netomi">
						Thomas Neidhart 
						<i class="fa fa-link" aria-hidden="true"></i>
					</a>
				</h3>
				<p></p>
				<dl>
					<dt><a href="https://accounts.eclipse.org/users/netomi">
						<i class="fa fa-user"></i> netomi (eclipse.org)
					</a></dt>
					<dt><a href="https://gitlab.eclipse.org/netomi">
						<i class="fa fa-gitlab"></i> netomi (gitlab.eclipse.org)
					</a></dt>
					<dt><a href="https://github.com/netomi">
						<i class="fa fa-github"></i> netomi (github.com)
					</a></dt>
					<dt><a href="https://netomi.github.io">
						<i class="fa fa-safari"></i> netomi.github.io
					</a></dt>
					<dt><a href="mailto:thomas.neidhart@eclipse-foundation.org">
						<i class="fa fa-envelope-o"></i> thomas.neidhart@eclipse-foundation.org
					</a></dt>
				</dl>
			</div>
		</div>
	</div>
	<hr/>

	<div class="media clearfix">
		<div class="col-xs-5 col-sm-4">
			<a name="" href="https://www.eclipse.org/user/fcojperez">
				<img class="img-thumbnail img-responsive margin-top-20" src="https://www.eclipse.org/org/foundation/images_staff/francisco-perez.jpg" alt="Francisco Peréz">
			</a>
		</div>
		<div class="col-xs-19 col-sm-20">
			<div class="media-body">
				<h3 class="margin-bottom-0">
					<a href="https://www.eclipse.org/user/fcojperez">
						Francisco Peréz 
						<i class="fa fa-link" aria-hidden="true"></i>
					</a>
				</h3>
				<p></p>
				<dl>
					<dt><a href="https://accounts.eclipse.org/users/fcojperez">
						<i class="fa fa-user"></i> fcojperez (eclipse.org)
					</a></dt>
					<dt><a href="https://gitlab.eclipse.org/fcojperez">
						<i class="fa fa-gitlab"></i> fcojperez (gitlab.eclipse.org)
					</a></dt>
					<dt><a href="https://github.com/fperezel">
						<i class="fa fa-github"></i> fperezel (github.com)
					</a></dt>
					<dt><a href="https://www.linkedin.com/in/fcojperez">
						<i class="fa fa-linkedin"></i> Francisco Peréz (LinkedIn)
					</a></dt>
					<dt><a href="mailto:francisco.perez@eclipse-foundation.org">
						<i class="fa fa-envelope-o"></i> francisco.perez@eclipse-foundation.org
					</a></dt>
				</dl>
			</div>
		</div>
	</div>
	<hr/>

	<div class="media clearfix">
		<div class="col-xs-5 col-sm-4">
			<a name="" href="https://www.eclipse.org/user/mrybczyn">
				<img class="img-thumbnail img-responsive margin-top-20" src="https://www.eclipse.org/org/foundation/images_staff/marta-rybczynska.jpg" alt="Marta Rybczynska">
			</a>
		</div>
		<div class="col-xs-19 col-sm-20">
			<div class="media-body">
				<h3 class="margin-bottom-0">
					<a href="https://www.eclipse.org/user/mrybczyn">
						Marta Rybczynska 
						<i class="fa fa-link" aria-hidden="true"></i>
					</a>
				</h3>
				<p></p>
				<dl>
					<dt><a href="https://accounts.eclipse.org/users/mrybczyn">
						<i class="fa fa-user"></i> mrybczyn (eclipse.org)
					</a></dt>
					<dt><a href="https://gitlab.eclipse.org/mrybczyn">
						<i class="fa fa-gitlab"></i> mrybczyn (gitlab.eclipse.org)
					</a></dt>
					<dt><a href="https://github.com/mrybczyn">
						<i class="fa fa-github"></i> mrybczyn (github.com)
					</a></dt>
					<dt><a href="mailto:marta.rybczynska@eclipse-foundation.org">
						<i class="fa fa-envelope-o"></i> marta.rybczynska@eclipse-foundation.org
					</a></dt>
				</dl>
			</div>
		</div>
	</div>
	<hr/>
	<div class="sectionbody">
		<div class="dlist">
			<dl>
			</dl>
		</div>
	</div>
</div>
