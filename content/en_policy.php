<?php
/**
 * Copyright (c) 2011 Eclipse Foundation and others.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Wayne Beaton (Eclipse Foundation)- initial API and implementation
 *    Christopher Guindon (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<div id="preamble">
	<div class="sectionbody">
		<div class="paragraph">
			<p>Version 1.1 February 4/2020</p>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="security-overview">
		<a class="anchor" href="#security-overview"></a><a class="link"
			href="#security-overview">Overview</a>
	</h2>
	<div class="sectionbody">
		<div class="paragraph">
			<p>
				The purpose of the Eclipse Vulnerability Reporting Policy is to set
				forth the general principles under which the Eclipse Foundation
				manages the reporting, management, discussion, and disclosure of
				Vulnerabilities discovered in Eclipse software. This Vulnerability
				Reporting Policy applies to all software distributed by the Eclipse
				Foundation, including all software authored by Eclipse Committers
				and third-parties. This Eclipse Vulnerability Reporting Policy
				should at all times be interpreted in a manner that is consistent
				with the Purposes of the Eclipse Foundation as set forth in the <a
					href="https://www.eclipse.org/org/documents/eclipse_foundation-bylaws.pdf">Eclipse
					Foundation Bylaws</a> and the <a
					href="https://www.eclipse.org/projects/dev_process/">Eclipse
					Foundation Development Process</a>.
			</p>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="security-terms">
		<a class="anchor" href="#security-terms"></a><a class="link"
			href="#security-terms">Terms</a>
	</h2>
	<div class="sectionbody">
		<div class="dlist">
			<dl>
				<dt class="hdlist1">Security Team</dt>
				<dd>
					<p>The Security Team, or "Eclipse Security Team" is the team tasked
						with security and Vulnerability management on behalf of the
						Eclipse community.</p>
				</dd>
				<dt class="hdlist1">Vulnerability</dt>
				<dd>
					<p>This policy uses the ISO 27005 definition of Vulnerability: "A
						weakness of an asset or group of assets that can be exploited by
						one or more threats."</p>
				</dd>
			</dl>
		</div>
		<div class="paragraph">
			<p>
				Other terms used in this document are defined in the <a
					href="https://www.eclipse.org/projects/dev_process/">Eclipse
					Foundation Development Process</a>.
			</p>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="security-team">
		<a class="anchor" href="#security-team"></a><a class="link"
			href="#security-team">Eclipse Security Team</a>
	</h2>
	<div class="sectionbody">
		<div class="paragraph">
			<p>The Eclipse Security Team is the first line of defense: it is
				effectively a triage unit with security and Vulnerability management
				expertise. The Security Team exists to provide assistance;
				Vulnerabilities are addressed and resolved by project committers
				with guidance and assistance from the Security Team.</p>
		</div>
		<div class="paragraph">
			<p>The Security Team is composed of a small number of security
				experts and representatives from the Project Management Committees.
				All members are appointed by EMO(ED) or their designate.</p>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="security-discussion">
		<a class="anchor" href="#security-discussion"></a><a class="link"
			href="#security-discussion">Discussion</a>
	</h2>
	<div class="sectionbody">
		<div class="paragraph">
			<p>The Eclipse Foundation is responsible for establishing
				communication channels for the Security Team.</p>
		</div>
		<div class="paragraph">
			<p>Every potential issue reported on established communication
				channels should be triaged and relevant parties notified. Initial
				discussion of a potential Vulnerability may occur privately amongst
				members of the project and Security Team. Discussion should be moved
				to and tracked by an Eclipse Foundation-supported issue tracker as
				early as possible once confirmed so the mitigation process may
				proceed. Appropriate effort must be undertaken to ensure the initial
				visibility, as well as the legitimacy, of every reported issue.</p>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="security-resolution">
		<a class="anchor" href="#security-resolution"></a><a class="link"
			href="#security-resolution">Resolution</a>
	</h2>
	<div class="sectionbody">
		<div class="paragraph">
			<p>A Vulnerability is considered resolved when either a patch or
				workaround is available, or it is determined that a fix is not
				possible or desirable.</p>
		</div>
		<div class="paragraph">
			<p>It is left to the discretion of the Security Team and Project
				Leadership Chain to determine what subset of the project team are
				best suited to resolve Vulnerabilities. The Security Team and
				project leaders may also&#8212;&#8203;at their
				discretion&#8212;&#8203;assemble external resources (e.g. subject
				matter experts) or call on the expertise of the Eclipse Architecture
				Council.</p>
		</div>
		<div class="paragraph">
			<p>
				In the unlikely event that a project team does not engage in good
				faith to resolve a disclosed Vulnerability, an Eclipse Foundation
				member may&#8212;&#8203;at their discretion&#8212;&#8203;engage in
				the Grievance Process as defined by the <a
					href="https://www.eclipse.org/projects/dev_process/">Eclipse
					Foundation Development Process</a>.
			</p>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="security-distribution">
		<a class="anchor" href="#security-distribution"></a><a class="link"
			href="#security-distribution">Distribution</a>
	</h2>
	<div class="sectionbody">
		<div class="paragraph">
			<p>Once a Vulnerability has been resolved, the updated software must
				be made available to the community.</p>
		</div>
		<div class="paragraph">
			<p>At a minimum, updated software must be made available via normal
				project distribution channels.</p>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="security-disclosure">
		<a class="anchor" href="#security-disclosure"></a><a class="link"
			href="#security-disclosure">Disclosure</a>
	</h2>
	<div class="sectionbody">
		<div class="paragraph">
			<p>Disclosure is initially limited to the reporter and all Eclipse
				Committers, but may be expanded to include other individuals.</p>
		</div>
		<div class="paragraph">
			<p>All Vulnerabilities must be disclosed, regardless of the
				resolution. Users and administrators of Eclipse software must be
				made aware that a Vulnerability exists so they may assess risk, and
				take the appropriate action to protect their users, servers and
				systems from potential exploit.</p>
		</div>
		<div class="sect2">
			<h3 id="security-timing">
				<a class="anchor" href="#security-timing"></a><a class="link"
					href="#security-timing">Timing</a>
			</h3>
			<div class="paragraph">
				<p>The timing of disclosure is left to the discretion of the Project
					Leadership Chain. In the absence of specific guidance from the
					Project Leadership Chain, the following guidelines are recommended:</p>
			</div>
			<div class="ulist">
				<ul>
					<li>
						<p>Vulnerabilities for which there is a patch, workaround or fix,
							should be disclosed to the community immediately; and</p>
					</li>
					<li>
						<p>Vulnerabilities&#8212;&#8203;regardless of
							state&#8212;&#8203;must be disclosed to the community after a
							maximum three months.</p>
					</li>
				</ul>
			</div>
			<div class="paragraph">
				<p>Vulnerabilities need not necessarily be resolved at the time of
					disclosure.</p>
			</div>
		</div>
		<div class="sect2">
			<h3 id="security-quiet-disclosure">
				<a class="anchor" href="#security-quiet-disclosure"></a><a
					class="link" href="#security-quiet-disclosure">Quiet Disclosure</a>
			</h3>
			<div class="paragraph">
				<p>
					A Vulnerability may be <em>quietly</em> disclosed by simply
					removing visibility restrictions.
				</p>
			</div>
			<div class="paragraph">
				<p>In general, quiet disclosure is appropriate only for issues that
					are identified by a committer as having been erroneously marked as
					Vulnerabilities.</p>
			</div>
		</div>
		<div class="sect2">
			<h3 id="security-progressive-disclosure">
				<a class="anchor" href="#security-progressive-disclosure"></a><a
					class="link" href="#security-progressive-disclosure">Progressive
					Disclosure</a>
			</h3>
			<div class="paragraph">
				<p>Knowledge of a Vulnerability can be extended to specific
					individuals before it is reported to the community. A Vulnerability
					may&#8212;&#8203;at the discretion of the committer&#8212;&#8203;be
					disclosed to specific individuals. A committer may, for example,
					provide access to a subject-matter expert to solicit help or
					advice. A Vulnerability may also be disclosed to known adopters to
					allow them an opportunity to mitigate their immediate risk and
					prepare for a forthcoming resolution.</p>
			</div>
		</div>
		<div class="sect2">
			<h3 id="security-full-disclosure">
				<a class="anchor" href="#security-full-disclosure"></a><a
					class="link" href="#security-full-disclosure">Full Disclosure</a>
			</h3>
			<div class="paragraph">
				<p>All Vulnerabilities must eventually be fully disclosed to the
					community at large.</p>
			</div>
			<div class="paragraph">
				<p>To complete the disclosure of a Vulnerability, all restrictions
					on visibility must be removed and the Vulnerability reported via
					channels provided by the Eclipse Foundation.</p>
			</div>
		</div>
		<div class="sect2">
			<h3 id="security-reporting">
				<a class="anchor" href="#security-reporting"></a><a class="link"
					href="#security-reporting">Reporting</a>
			</h3>
			<div class="paragraph">
				<p>A project team may, at their discretion, opt to disclose a
					Vulnerability to a reporting authority.</p>
			</div>
			<div class="paragraph">
				<p>The EMO will determine how to engage with Vulnerability reporting
					authorities.</p>
			</div>
		</div>
	</div>
</div>
<div class="sect1">
	<h2 id="history">
		<a class="anchor" href="#history"></a><a class="link" href="#history">History</a>
	</h2>
	<div class="sectionbody">
		<div class="paragraph">
			<p>Changes made in this document:</p>
		</div>
		<div class="sect2">
			<h3 id="changelog">
				<a class="anchor" href="#changelog"></a><a class="link"
					href="#changelog">ChangeLog</a>
			</h3>
			<div class="sect3">
				<h4 id="2019-2019-03-06-version-1-1">
					<a class="anchor" href="#2019-2019-03-06-version-1-1"></a><a
						class="link" href="#2019-2019-03-06-version-1-1">[2019] -
						2019-03-06 (version 1.1)</a>
				</h4>
				<div class="sect4">
					<h5 id="changes">
						<a class="anchor" href="#changes"></a><a class="link"
							href="#changes">Changes</a>
					</h5>
					<div class="ulist">
						<ul>
							<li>
								<p>Changed the name from "Security Policy" to "Vulnerability
									Reporting Policy"</p>
							</li>
							<li>
								<p>Formalized terms into their own section.</p>
							</li>
							<li>
								<p>Changed several occurances of the word "can" to "may" to
									improve clarity.</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="sect4">
					<h5 id="added">
						<a class="anchor" href="#added"></a><a class="link" href="#added">Added</a>
					</h5>
					<div class="ulist">
						<ul>
							<li>
								<p>Added a pointer to the Grievance Handling section of the
									Eclipse Foundation Development Process.</p>
							</li>
						</ul>
					</div>
				</div>
				<div class="sect4">
					<h5 id="removed">
						<a class="anchor" href="#removed"></a><a class="link"
							href="#removed">Removed</a>
					</h5>
					<div class="ulist">
						<ul>
							<li>
								<p>Removed references to specific technology (e.g., Bugzilla or
									specific mailing lists). These are implementation details.</p>
							</li>
							<li>
								<p>Removed references to the Eclipse Planning Council and
									Simultaneous Release.</p>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>